import { Component } from '@angular/core';

import * as treeData from "./../data/mock-tree.json";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'WebIDE';

  root: ITreeNode = treeData;
  //content!: IContent;

}
