import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { ExplorerComponent } from './components/explorer/explorer.component';
import { EditorComponent } from './components/editor/editor.component';
import { NodeComponent } from './components/node/node.component'; // For Text-Area

import { FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // Contains ngModel

import { MatCardModule } from "@angular/material/card"; // For Card-like Layout
import { MatButtonModule } from "@angular/material/button"; // Button Styles
import { MatInputModule } from "@angular/material/input";


@NgModule({ // Groupd related functionality and
  declarations: [ // Declare items that Module contains (e.g. Components, Directives, Pipes)
    AppComponent, ExplorerComponent, EditorComponent, NodeComponent
  ],
  imports: [ // Import other Modules
    BrowserModule, FormsModule, BrowserAnimationsModule,
    MatCardModule, MatButtonModule, MatInputModule
  ],
  providers: [], // Services available to Module -- Effectively "Dependency Injection"
  bootstrap: [AppComponent] // Main Component where all else stems from
})
export class AppModule {

}
