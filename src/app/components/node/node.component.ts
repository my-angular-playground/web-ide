import { Component, Input } from '@angular/core';

import { ContentService } from "../../services/content.service";

@Component({
  selector: 'app-node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.css']
})
export class NodeComponent {
  @Input() node!: ITreeNode; // Definite Assignment Operator ! -- "It will exist"
  @Input() isRoot: boolean = false; // Defaulted so only need specify "true" on Root // TODO: Consider if wish this approach or use "level" which may allow for future use (but takes more Memory per Node)

  isSelected: boolean = false;

  // Due to Angular Services being Singletons -> SAME Service Injected to all Nodes (especially since "providedIn" in Service = "root", i.e. Singleton for Whole App)
  constructor( private contentService: ContentService ) {} // Dependency Injection via Parameter Properties

  toggleSelection() {
    // TODO: Add some logic to allow a file to be selected multi-tims and not "deselect" (i.e. must close file to effectively "deselect")
    this.isSelected = !this.isSelected;

    if( !this.isDirectory() && this.isSelected ) {
      // Pass "content" Object of Selected Node to ContentService (which Editor Component reads from)
      // This is the same Object in Memory as Node contains, since Editor assigns its "content" Property to this (same Object), changes in Editor persist to this Node -> Effectively maintains changes between files
      // KEY: This comes from JavaScript's "Pass by Reference" for Objects (and Arrays)
      // -- BECAUSE OF THIS, [(ngmodel)]="content.text" in Editor updates "content.text" of Editor Component AND associated "content" Object in Node Component -- BECAUSE they Reference the same Object
      this.contentService.updateContent( this.node.content );
    }
  }

  isDirectory() {
    return this.node.children != null;
  }
}
