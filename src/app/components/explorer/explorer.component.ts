import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-explorer',
  templateUrl: './explorer.component.html',
  styleUrls: ['./explorer.component.css']
})
export class ExplorerComponent {
  @Input() root!: ITreeNode; // Definite Assignment Operator ! -- "It will exist"
}
