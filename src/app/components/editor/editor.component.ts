import { Component, OnInit, OnDestroy } from '@angular/core';

import { Observable, Subscription } from "rxjs";

import { ContentService } from "../../services/content.service";

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit, OnDestroy { // Interface to enforce desired Implementation (a "Contract" to abide by)

  content!: IContent; // Definite Assignment Operator
  private contentSubscription!: Subscription; // Subscription for Content Observable (BehaviorSubject)

  constructor( private contentService: ContentService ) {} // Dependency Injection via Parameter Properties



  ngOnInit() {
    this.contentSubscription = this.contentService.content$.subscribe( content => {
      // Points to same Objct in Memory as Node's "content" Field (thus, persists changes between Switching Files)
      this.content = content;
    });
  }

  ngOnDestroy() {
    if( this.contentSubscription ) {
      this.contentSubscription.unsubscribe(); // Manual Unsubscribe due to manual Subscribe // TODO: Consider Async Pipe
    }
  }

}
