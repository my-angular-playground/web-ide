import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  private _defaultContent = {
    text: "WELCOME - Select a file to begin (:"
  }

  // BehaviorSubject maintains latest ("current") Value, which is emitted to all new Subscribers (many)
  // Alternative -> Regular Subject, but new Subscribers not get value immediately
  // Alternative -> No Observable, but clients not able "react" to changes in value (i.e. Editor and Node not able to communicate via this Service)
  private _content = new BehaviorSubject<IContent>( this._defaultContent ); // Hard-init due to strictPropertyInitialization true by default for TypeScript Strict Mode (I prefer to keep it on)

  // This is the interface clients use.
  // Observable only is exposed to enforce updateContent as only means to update "content" (prevents .next(), .error(), etc from clients)
  content$: Observable<IContent> = this._content.asObservable();

  updateContent( newContent: IContent | undefined  ) {
    if( newContent == null ) { // Can effectively serve as an "Unselect" or Close (last) File
      this._content.next( this._defaultContent );
      return;
    }

    this._content.next( newContent );
  }
}
