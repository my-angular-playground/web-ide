
// TODO: Consider a separate Directory and File Interface (may prevent e.g. "Directory having content", or "File having children", which doesn't (necessarily) make sense but is allowed via this Interface)
interface ITreeNode {
  name: string;

  content?: IContent; // If File
  children?: ITreeNode[]; // If Folder
}
