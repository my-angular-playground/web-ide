# WebIDE

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.6.

## Overview

### **DEMO**
![WebIDE_Demo](https://gitlab.com/my-angular-playground/web-ide/-/raw/master/WebIDE_Demo.mp4?ref_type=heads)


**NOTE**: Right now, more of a Text Editor than an IDE...

Features not supported yet:
- Machine Learning Model interactions (e.g. Icon to add Prompt and insert generated text retrieved via API)
- IDE Features (e.g. Debugger, Profiler, Code Completion, Compiler/Interpreter, etc)
- Saving Files (persisting changes between revisits)
- Tabs, multiple file edits
- Syntax Highlighting
- Server Interactions
- Built-in Terminal
- ...




# Angular Specifics

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
